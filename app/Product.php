<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";
    public function productValues(){
        return $this->hasMany('App\Product_value');
    }

    public function category(){
        return $this->belongsTo('category');
    }

    public function property(){
        return $this->belongsToMany('App\Property', 'product_values');
    }
}
