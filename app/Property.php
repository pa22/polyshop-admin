<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = "properties";
    public function productValues(){
        return $this->hasMany('App\Product_value');
    }
    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function product(){
        return $this->belongsToMany('App\Product', 'product_values');
    }
}
