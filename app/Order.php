<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = "orders";
    public function username(){
        return $this->belongsTo('username');
    }
    public function product(){
        return $this->belongsTo('product');
    }
}
