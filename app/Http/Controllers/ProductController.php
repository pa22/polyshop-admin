<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($catid, $id)
    {
        $r=0;
		if(Auth::check()){
			$i=Auth::id();
			$u=\App\Userrole::where('user_id', $i)->get();
			if (sizeof($u)>0){
				foreach($u as $k){
					if($k->role_id == 1){
						$r=1;
					}
				}
			}
		}
        $prov = \App\Product_value::where('product_id',$id)->get();
        return view('category.product.list',['values' => $prov, 'r' => $r, 'catid' => $catid]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($catid, $id)
    {
        $prod = \App\Product::find($id);
        return view('category.product.edit',['product' => $prod]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$catid, $id)
    {
        $p = \App\Product::find($id);
        $p->name = $request->name;
        $p->save();
        return redirect('/home/category/'.$catid);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($catid, $id)
    {
        \App\Product::destroy($id);
        return redirect('/home/category/'.$catid);
    }
}
