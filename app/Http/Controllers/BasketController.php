<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BasketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $r=0;
		if(Auth::check()){
			$i=Auth::id();
			$u=\App\Userrole::where('user_id', $i)->get();
			if (sizeof($u)>0){
				foreach($u as $k){
					if($k->role_id == 1){
						$r=1;
					}
				}
			}
		}
        $arBas = \App\Basket::all();
        return view('basket.list',['baskets' => $arBas, 'r' => $r]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $basket = \App\Basket::find($id);
        return view('basket.edit',['basket' => $arTournament]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $b = \App\Basket::find($id);
        $b->username_id = $request->username_id;
        $b->product_id = $request->product_id;
        $b->number = $request->number;
        $b->save();
        return redirect('/home/basket');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Basket::destroy($id);
        return redirect('/home/basket');
    }
}
