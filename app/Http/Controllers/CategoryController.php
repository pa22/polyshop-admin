<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $r=0;
		if(Auth::check()){
			$i=Auth::id();
			$u=\App\Userrole::where('user_id', $i)->get();
			if (sizeof($u)>0){
				foreach($u as $k){
					if($k->role_id == 1){
						$r=1;
					}
				}
			}
		}
        $arCat = \App\Category::all();
        return view('category.list',['categories' => $arCat, 'r' => $r]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $r=0;
		if(Auth::check()){
			$i=Auth::id();
			$u=\App\Userrole::where('user_id', $i)->get();
			if (sizeof($u)>0){
				foreach($u as $k){
					if($k->role_id == 1){
						$r=1;
					}
				}
			}
		}
        $prod = \App\Product::where('category_id', $id)->get();
        $prop = \App\Property::where('category_id', $id)->get();
        return view('category.detail',['products' => $prod, 'properties' => $prop, 'r' => $r]); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = \App\Category::find($id);
        return view('category.edit',['category' => $cat]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $c = \App\Category::find($id);
        $c->name = $request->name;
        $c->save();
        return redirect('/home/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Category::destroy($id);
        return redirect('/home/category');
    }
}
