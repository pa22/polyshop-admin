@include('header')
<div class="jumbotron jumbotron-fluid">
  <div class="container">
  	@if(!Auth::check())
    <h1 class="display-3">Добро пожаловать</h1>
    <p class="lead">Для начала необходимо войти в аккаунт (или зарегистрироваться).</p>
	<p><a href="{{ route('login') }}" class="btn btn-dark">Войти</a></p>
	<p><a href="{{ route('register') }}" class="btn btn-dark">Зарегистрироваться</a></p>
	@else
	<h1 class="display-3">Добро пожаловать, {{Auth::user()->name}} :)</h1>
    <p class="lead">Вы уже вошли!</p>
    <p>Выберите таблицу ниже</p>
    <p><a class="btn btn-dark" href="/home/order">Orders</a></p>
    <p><a class="btn btn-dark" href="/home/basket">Basket</a></p>
    <p><a class="btn btn-dark" href="/home/category">Categories</a></p>
	@endif
  </div>
</div>
@include ('footer')