@include('header')
<form method="POST" action="/home/order/{{$basket->id}}">
    <table class="table table-dark">
        @csrf
        {{ method_field ('PUT') }}
        <tr>
            <td>Username id</td>
            <td>
                <input name="username_id" value="{{$basket->username_id}}">
            </td>
            <td>Product id</td>
            <td>
                <input name="product_id" value="{{$basket->product_id}}">
            </td>
            <td>Number</td>
            <td>
                <input name="number" value="{{$basket->number}}">
            </td>
        </tr>
    </table>
    <br>
    <button class="btn btn-success" type="submit">Сохранить</button>
</form>
<br><br><br>
<a href="/home/basket" class="btn btn-primary">Назад</a>
@include ('footer')