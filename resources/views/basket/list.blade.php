@include('header')
<br>
<h2>Basket</h2>
<br>
@if($r==1)
<!--    <a href="/home/tournament/create" class="btn btn-warning">Создать Элемент</a>-->
	@endif
    <br><br>
    <table class="table table-dark">
        <tr>
            <td>Username id</td>
            <td>Product id</td>
            <td>Number</td>
            <td></td>
            <td></td>
        </tr>
        @foreach ($baskets as $basket)
            <tr>
                <td class="col-3">{{$basket->username_id}}</td>
                <td class="col-3">{{$basket->product_id}}</td>
                <td class="col-3">{{$basket->number}}</td>
				@if($r==1)
                <td class="col-2"><a href="/home/basket/{{$basket->id}}/edit" class="btn btn-outline-info">Редактировать</a></td>
                <td class="col">
                    <form action="/home/basket/{{$basket->id}}" method="POST">
                        {{csrf_field()}}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger" type="submit">Удалить</button>
                    </form>
                </td>
				@endif
            </tr>
        @endforeach
    </table>
    <br><br>
    <a href="/home" class="btn btn-primary">Назад</a>
    <br><br><br><br><br>
@include ('footer')