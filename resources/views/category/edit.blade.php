@include('header')
<form method="POST" action="/home/category/{{$category->id}}">
    <table class="table table-dark">
        @csrf
        {{ method_field ('PUT') }}
        <tr>
            <td>Name</td>
            <td>
                <input name="name" value="{{$category->name}}">
            </td>
        </tr>
    </table>
    <br>
    <button class="btn btn-success" type="submit">Сохранить</button>
</form>
<br><br><br>
<a href="/home/category" class="btn btn-primary">Назад</a>
@include ('footer')