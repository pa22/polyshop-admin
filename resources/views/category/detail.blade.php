@include('header')
@if($r==1)
<!--    <a href="/home/tournament/create" class="btn btn-warning">Создать Элемент</a>-->
	@endif
    <br><br>
    <h2>Products</h2>
    <table class="table table-dark">
        <tr>
            <td>Name</td>
            <td></td>
            <td></td>
        </tr>
        @foreach ($products as $product)
            <tr class="d-flex">
                <td class="col-9"><a href="/home/category/{{$product->category_id}}/product/{{$product->id}}">{{$product->name}}</a></td>
				@if($r==1)
                <td class="col-2"><a href="/home/category/{{$product->category_id}}/product/{{$product->id}}/edit" class="btn btn-outline-info">Редактировать</a></td>
                <td class="col">
                    <form action="/home/category/{{$product->category_id}}/product/{{$product->id}}" method="POST">
                        {{csrf_field()}}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger" type="submit">Удалить</button>
                    </form>
                </td>
				@endif
            </tr>
        @endforeach
    </table>
    <br><br>
       <h2>Properties</h2>
        <table class="table table-dark">
        <tr class="d-flex">
            <td class="col-8">Name</td>
            <td></td>
            <td></td>
        </tr>
        @foreach ($properties as $property)
            <tr class="d-flex">
                <td class="col-8">{{$property->name}}</td>
				@if($r==1)
                <td class="col-2"><a href="/home/category/{{$property->category_id}}/property/{{$property->id}}/edit" class="btn btn-outline-info">Редактировать</a></td>
                <td class="col">
                    <form action="/home/category/{{$property->category_id}}/property/{{$property->id}}" method="POST">
                        {{csrf_field()}}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger" type="submit">Удалить</button>
                    </form>
                </td>
				@endif
            </tr>
        @endforeach
    </table>
    <br><br>
 <a href="/home/category/" class="btn btn-primary">Назад</a>
 <br><br><br><br><br>
@include ('footer')