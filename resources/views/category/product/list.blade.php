@include('header')
<br>
<h2>Property values for the current product</h2>
<br>
@if($r==1)
<!--    <a href="/home/tournament/create" class="btn btn-warning">Создать Элемент</a>-->
	@endif
    <br><br>
    <table class="table table-dark">
        <tr class="d-flex">
            <td class="col-3">Property id</td>
            <td class="col-5">Value</td>
            <td></td>
            <td></td>
        </tr>
        @foreach ($values as $value)
            <tr class="d-flex">
                <td class="col-3">{{$value->property_id}}</td>
                <td class="col-5">{{$value->property_value}}</td>
				@if($r==1)
                <td class="col-2"><a href="/home/category/{{$catid}}/product/{{$value->product_id}}/value/{{$value->id}}/edit" class="btn btn-outline-info">Редактировать</a></td>
                <td class="col">
                    <form action="/home/category/{{$catid}}/product/{{$value->product_id}}/value/{{$value->id}}" method="POST">
                        {{csrf_field()}}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger" type="submit">Удалить</button>
                    </form>
                </td>
				@endif
            </tr>
        @endforeach
    </table>
    <br><br>
    <a href="/home/category/{{$catid}}" class="btn btn-primary">Назад</a>
    <br><br><br><br><br>
@include ('footer')