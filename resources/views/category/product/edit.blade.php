@include('header')
<form method="POST" action="/home/category/{{$product->category_id}}/product/{{$product->id}}">
    <table class="table table-dark">
        @csrf
        {{ method_field ('PUT') }}
        <tr>
            <td>Name</td>
            <td>
                <input name="name" value="{{$product->name}}">
            </td>
        </tr>
    </table>
    <br>
    <button class="btn btn-success" type="submit">Сохранить</button>
</form>
<br><br><br>
<a href="/home/category/{{$product->category_id}}" class="btn btn-primary">Назад</a>
@include ('footer')