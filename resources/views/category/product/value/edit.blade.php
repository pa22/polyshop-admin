@include('header')
<form method="POST" action="/home/category/{{$catid}}/product/{{$value->product_id}}/value/{{$value->id}}">
    <table class="table table-dark">
        @csrf
        {{ method_field ('PUT') }}
        <tr>
            <td>Property id</td>
            <td>
                <input name="property_id" value="{{$value->property_id}}">
            </td>
            <td>Property value</td>
            <td>
                <input name="property_value" value="{{$value->property_value}}">
            </td>
        </tr>
    </table>
    <br>
    <button class="btn btn-success" type="submit">Сохранить</button>
</form>
<br><br><br>
<a href="/home/category/{{$catid}}/product/{{$value->product_id}}" class="btn btn-primary">Назад</a>
@include ('footer')