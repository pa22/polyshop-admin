@include('header')
@if($r==1)
<!--    <a href="/home/tournament/create" class="btn btn-warning">Создать Элемент</a>-->
	@endif
    <br><br>
    <table class="table table-dark">
        <tr>
            <td>Name</td>
            <td></td>
            <td></td>
        </tr>
        @foreach ($categories as $category)
            <tr class="d-flex">
                <td class="col-9"><a class="mainlink" href="/home/category/{{$category->id}}">{{$category->name}}</a></td>
				@if($r==1)
                <td class="col-2"><a href="/home/category/{{$category->id}}/edit" class="btn btn-outline-info">Редактировать</a></td>
                <td class="col">
                    <form action="/home/category/{{$category->id}}" method="POST">
                        {{csrf_field()}}
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger" type="submit">Удалить</button>
                    </form>
                </td>
				@endif
            </tr>
        @endforeach
    </table>
    <br><br>
    <a href="/home/" class="btn btn-primary">Назад</a>
    <br><br><br><br><br>
@include ('footer')