@include('header')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <p>{{Auth::user()->name}}, Вы успешно вошли!</p>
					<p>Выберите таблицу ниже</p>
                    <p><a class="btn btn-dark" href="/home/order">Orders</a></p>
                    <p><a class="btn btn-dark" href="/home/basket">Basket</a></p>
                    <p><a class="btn btn-dark" href="/home/category">Categories</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@include ('footer')
